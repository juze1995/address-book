# address-book

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

本demo仿照手机通讯录，可实现首字母查询，模糊搜索；点击侧边字母可快速定位到首字母区域；支持查询一天内的搜索记录，搜索记录存放于sessionStorage里，每次键入搜索
都会去重，同时会更新最新一次键入时间；点击某条联系人时，会弹出详情模态框展示联系人详细信息，详情页支持上下切换用户。
本demo技术栈 vue2+element-ui，同时引用到的插件有better-scroll，用于模拟手机端滑动。
