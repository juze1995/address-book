const enums = {
  // 播控任务监看状态
  'A': [{
      name: "Angela",
      email: "ANgela@xxxx.com",
      followers: 0,
      starts: 0,
    },
    {
       name: "Ana",
      email: "Ana@xxxx.com",
      followers: 1,
      starts: 1,
    },
    {
       name: "anchor",
      email: "anchor@xxxx.com",
      followers: 2,
      starts: 2,
    }
  ],
  'B': [{
      name: "Bob",
      email: "Bob@xxxx.com",
      followers: 0,
      starts: 0,
    },
    {
       name: "BMW",
      email: "BMW@xxxx.com",
      followers: 1,
      starts: 1,
    },
    {
       name: "BB",
      email: "BB@xxxx.com",
      followers: 2,
      starts: 2,
    }
  ],
  'C': [{
      name: "Chen",
      email: "Chen@xxxx.com",
      followers: 0,
      starts: 0,
    },
    {
       name: "Ciao",
      email: "Ciao@xxxx.com",
      followers: 1,
      starts: 1,
    }
  ],
  'E': [{
      name: "Earth",
      email: "Earth@xxxx.com",
      followers: 0,
      starts: 0,
    },
    {
       name: "EU",
      email: "EU@xxxx.com",
      followers: 1,
      starts: 1,
    },
    {
       name: "EChart",
      email: "EChart@xxxx.com",
      followers: 2,
      starts: 2,
    }
  ],
  'F': [{
      name: "Frank",
      email: "Frank@xxxx.com",
      followers: 0,
      starts: 0,
    }
  ],
};
export default enums
